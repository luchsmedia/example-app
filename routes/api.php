<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Airline;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('airlines', function() {
    return Airline::all();
});

Route::get('airlines/{id}', function($id) {
    return Airline::find($id);
});

Route::post('airlines', function(Request $request) {
    return Airline::create($request->all());
});

Route::put('airlines/{id}', function(Request $request, $id) {
    $airline = Airline::findOrFail($id);
    $airline->update($request->all());
    return $airline;
});

Route::get('passengers/{id}', function($id) {
    $json = file_get_contents('https://api.instantwebtools.net/v1/passenger?page=' . $_GET['page'] . '&size=50');
    $data = json_decode($json, false)->data;
    return response()->json($data);
});