<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Airline;

class AirlinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('airlines')->delete();
        $json = file_get_contents('https://api.instantwebtools.net/v1/airlines');
        $data = json_decode($json, false);
        foreach ($data as $obj) {
            if (isset($obj->id)
                and isset($obj->country)
                and isset($obj->head_quaters)) {
                Airline::create(array(
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'country' => $obj->country,
                    'logo' => $obj->logo,
                    'slogan' => $obj->slogan,
                    'head_quaters' => $obj->head_quaters,
                    'website' => $obj->website,
                    'established' => $obj->established
                ));
            }
        }

    }
}
