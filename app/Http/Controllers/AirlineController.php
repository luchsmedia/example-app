<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AirlineController extends Controller
{
    public function index() {
        return \App\Models\Airline::all();
    }

    public function show($id) {
        return \App\Models\Airline::find($id);
    }

    public function store(Request $request) {
        return \App\Models\Airline::create($request->all());
    }

    public function update(Request $request, $id) {
        $airline = Airline::findOrFail($id);
        $airline->update($request->all());
        return $airline;
    }
}
